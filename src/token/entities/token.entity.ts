import {
  Column,
  Entity,
  ObjectID,
  ObjectIdColumn,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class Token {
  @ObjectIdColumn()
  id: ObjectID;

  @ObjectIdColumn()
  userId: ObjectID;

  @Column()
  token: string;

  @CreateDateColumn()
  createdDate: Date;

  @UpdateDateColumn()
  modifiedData: Date;
}
