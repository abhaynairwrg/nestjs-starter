import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ObjectID } from 'mongodb';
import { MongoRepository } from 'typeorm';
import { Token } from './entities/token.entity';

@Injectable()
export class TokenService {
  constructor(
    @InjectRepository(Token) private tokenRepository: MongoRepository<Token>,
  ) {}

  async create(tokenData: { userId: string; token: string }) {
    try {
      const userExists = await this.tokenRepository.findOne({
        where: {
          userId: {
            $eq: tokenData.userId,
          },
        },
      });

      if (!userExists) {
        return await this.tokenRepository.save(tokenData);
      }

      return await this.tokenRepository.update(
        {
          id: userExists.id,
        },
        {
          token: tokenData.token,
        },
      );
    } catch (error) {
      throw error;
    }
  }

  async findOne(token: string): Promise<Token> {
    try {
      return await this.tokenRepository.findOne({
        where: {
          token: { $eq: token },
        },
      });
    } catch (error) {
      throw error;
    }
  }

  async remove(userId: string) {
    try {
      return await this.tokenRepository.findOneAndDelete({
        userId: new ObjectID(userId),
      });
    } catch (error) {
      throw error;
    }
  }
}
