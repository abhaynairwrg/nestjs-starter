import { diskStorage } from 'multer';
import { nanoid } from 'nanoid';
const path = require('path');

type IValidMimeType = 'image/png' | 'image/jpg' | 'image/jpeg';

const validMimeTypes: IValidMimeType[] = [
  'image/png',
  'image/jpg',
  'image/jpeg',
];

export const saveImageToStorage = {
  storage: diskStorage({
    destination: './images',
    filename: (req, file, cb) => {
      const fileExtension: string = path.extname(file.originalname);
      const fileName: string = nanoid() + fileExtension;

      cb(null, fileName);
    },
  }),
  fileFilter: (req, file, cb) => {
    const allowedFileTypes: IValidMimeType[] = validMimeTypes;
    allowedFileTypes.includes(file.mimetype) ? cb(null, true) : cb(null, false);
  },
};
