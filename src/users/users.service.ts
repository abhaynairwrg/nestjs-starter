import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ObjectID } from 'mongodb';
import { MongoRepository } from 'typeorm';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User) private usersRepository: MongoRepository<User>,
  ) {}

  async create(createUserDto: CreateUserDto): Promise<User> {
    try {
      return await this.usersRepository.save(createUserDto);
    } catch (error) {
      throw error;
    }
  }

  async findAll() {
    try {
      return await this.usersRepository.find({
        select: ['name', 'email', 'role', 'lastLoggedIn', 'isActive'],
      });
    } catch (error) {
      throw error;
    }
  }

  async findById(id: string) {
    try {
      return await this.usersRepository.findOne({
        where: {
          _id: { $eq: new ObjectID(id) },
        },
        select: ['name', 'email', 'role', 'lastLoggedIn', 'isActive', 'avatar'],
      });
    } catch (error) {
      throw error;
    }
  }

  async findByEmail(email: string) {
    try {
      return await this.usersRepository.findOne({
        where: {
          email: { $eq: email },
        },
        select: [
          'name',
          'email',
          'role',
          'lastLoggedIn',
          'isActive',
          'password',
          'avatar',
        ],
      });
    } catch (error) {
      throw error;
    }
  }

  async update(id: string, updateUserDto: UpdateUserDto) {
    try {
      return await this.usersRepository.update(
        {
          id: new ObjectID(id),
        },
        updateUserDto,
      );
    } catch (error) {
      throw error;
    }
  }

  async remove(id: string) {
    try {
      return await this.usersRepository.delete({
        id: new ObjectID(id),
      });
    } catch (error) {
      throw error;
    }
  }
}
