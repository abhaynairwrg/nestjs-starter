import {
  Column,
  Entity,
  ObjectID,
  ObjectIdColumn,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class User {
  @ObjectIdColumn()
  id: ObjectID;

  @Column({ nullable: true })
  avatar: string;

  @Column()
  name: string;

  @Column()
  email: string;

  @Column({
    enum: ['ADMIN', 'USER'],
  })
  role: string;

  @Column()
  password: string;

  @Column({
    default: true,
  })
  isActive: boolean;

  @Column({
    type: 'date',
  })
  lastLoggedIn: Date;

  @CreateDateColumn()
  createdDate: Date;

  @UpdateDateColumn()
  modifiedData: Date;
}
