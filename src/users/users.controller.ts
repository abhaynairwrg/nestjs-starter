import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UnprocessableEntityException,
  UseInterceptors,
  UploadedFile,
  ConflictException,
  Request,
  NotFoundException,
} from '@nestjs/common';
import { UsersService } from './users.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { hash } from 'bcryptjs';
import { ConfigService } from '@nestjs/config';
import { FileInterceptor } from '@nestjs/platform-express';
import { Public } from 'src/auth/role/public.decorator';
import { saveImageToStorage } from 'src/helpers/image-storage';
import { TokenService } from 'src/token/token.service';
import { unlink } from 'fs';
import { join } from 'path/posix';

@Controller('users')
export class UsersController {
  constructor(
    private readonly usersService: UsersService,
    private configService: ConfigService,
    private readonly tokeService: TokenService,
  ) {}

  @Public()
  @Post()
  @UseInterceptors(FileInterceptor('avatar', saveImageToStorage))
  async create(
    @Body() createUserDto: CreateUserDto,
    @UploadedFile() avatar: Express.Multer.File,
  ) {
    try {
      const { password } = createUserDto;

      const saltOrRounds = +this.configService.get<number>('AUTH_SALT_ROUNDS');
      const hashedPassword = await hash(password, saltOrRounds);

      const userExists = await this.usersService.findByEmail(
        createUserDto.email,
      );

      if (userExists) {
        throw new ConflictException('Email already exists');
      }

      await this.usersService.create({
        ...createUserDto,
        password: hashedPassword,
        avatar: avatar.filename,
      });
    } catch (error) {
      throw error;
    }
  }

  @Get()
  async findAll() {
    try {
      return await this.usersService.findAll();
    } catch (error) {
      throw error;
    }
  }

  @Get(':id')
  async findOne(@Param('id') id: string) {
    try {
      const user = await this.usersService.findById(id);

      if (!user) {
        throw new NotFoundException();
      }

      return user;
    } catch (error) {
      throw error;
    }
  }

  @Patch(':id')
  async update(@Param('id') id: string, @Body() updateUserDto: UpdateUserDto) {
    try {
      if (JSON.stringify(updateUserDto) === '{}') {
        throw new UnprocessableEntityException();
      }

      const userExists = await this.findOne(id);

      if (!userExists) {
        throw new NotFoundException();
      }

      await this.usersService.update(id, updateUserDto);
    } catch (error) {
      throw error;
    }
  }

  @Delete(':id')
  async remove(@Param('id') id: string, @Request() req) {
    try {
      const userExists = await this.findOne(id);

      if (!userExists) {
        throw new NotFoundException();
      }

      if (userExists.avatar) {
        unlink(join('./images', userExists.avatar), (err) => {
          console.log(err);
        });
      }

      await this.usersService.remove(id);
      await this.tokeService.remove(req.user.id);
    } catch (error) {
      throw error;
    }
  }
}
