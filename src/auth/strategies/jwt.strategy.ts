import { ForbiddenException, Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Request } from 'express';
import { ExtractJwt, Strategy, StrategyOptions } from 'passport-jwt';
import { TokenService } from 'src/token/token.service';

@Injectable()
export class JWTStrategy extends PassportStrategy(Strategy) {
  constructor(private tokenService: TokenService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: process.env.AUTH_JWT_SECRET,
      passReqToCallback: true,
    } as StrategyOptions);
  }

  async validate(req: Request, payload: any) {
    try {
      const rawToken = req.headers['authorization'].split(' ')[1];

      const tokenExists = await this.tokenService.findOne(rawToken);

      if (!tokenExists) {
        throw new ForbiddenException('Token Expired');
      }

      return {
        id: payload.sub,
        email: payload.email,
        role: payload.role,
      };
    } catch (error) {
      throw error;
    }
  }
}
