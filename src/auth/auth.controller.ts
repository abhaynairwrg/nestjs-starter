import {
  Body,
  Controller,
  Delete,
  Post,
  Request,
  UseGuards,
} from '@nestjs/common';
import { AuthService } from './auth.service';
import { LoginDto } from './dto/login.dto';
import { LocalAuthGuard } from './guards/localauth.guard';
import { Public } from './role/public.decorator';

@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @Public()
  @UseGuards(LocalAuthGuard)
  @Post('login')
  async login(@Body() loginDto: LoginDto, @Request() req) {
    try {
      const { access_token } = await this.authService.login(req.user);

      return {
        access_token,
        userData: {
          id: req.user.id,
          email: req.user.email,
          role: req.user.role,
        },
      };
    } catch (error) {
      throw error;
    }
  }

  @Delete('logout')
  async logout(@Request() req) {
    try {
      await this.authService.logout(req.user.id);
    } catch (error) {
      throw error;
    }
  }
}
