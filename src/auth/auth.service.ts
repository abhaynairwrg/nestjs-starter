import { Injectable } from '@nestjs/common';
import { UsersService } from 'src/users/users.service';
import { compare } from 'bcryptjs';
import { JwtService } from '@nestjs/jwt';
import { TokenService } from 'src/token/token.service';

@Injectable()
export class AuthService {
  constructor(
    private userService: UsersService,
    private jwtService: JwtService,
    private tokenService: TokenService,
  ) {}

  async validateUser(email: string, password: string) {
    try {
      const user = await this.userService.findByEmail(email);

      const passwordsAreSame = await compare(password, user.password);

      if (user && passwordsAreSame) {
        const { password: userPass, ...rest } = user;

        return rest;
      }

      return null;
    } catch (error) {
      throw error;
    }
  }

  async login(user: any) {
    try {
      const payload = { email: user.email, sub: user.id, role: user.role };

      const access_token = this.jwtService.sign(payload);

      await this.tokenService.create({
        userId: user.id,
        token: access_token,
      });

      return {
        access_token,
      };
    } catch (error) {
      throw error;
    }
  }

  async logout(userId: string) {
    try {
      await this.tokenService.remove(userId);
    } catch (error) {
      throw error;
    }
  }
}
