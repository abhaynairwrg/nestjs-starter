import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { MongoRepository } from 'typeorm';
import { CreateNoteDto } from './dto/create-note.dto';
import { UpdateNoteDto } from './dto/update-note.dto';
import { Note } from './entities/note.entity';
import { ObjectID } from 'mongodb';

@Injectable()
export class NotesService {
  constructor(
    @InjectRepository(Note) private notesRepository: MongoRepository<Note>,
  ) {}

  async create(createNoteDto: CreateNoteDto) {
    try {
      return await this.notesRepository.save(createNoteDto);
    } catch (error) {
      throw error;
    }
  }

  async findAll(): Promise<Note[]> {
    try {
      return await this.notesRepository.find();
    } catch (error) {
      throw error;
    }
  }

  async findOne(id: string): Promise<Note> {
    try {
      return await this.notesRepository.findOne({
        where: {
          _id: { $eq: new ObjectID(id) },
        },
      });
    } catch (error) {
      throw error;
    }
  }

  async update(id: string, updateNoteDto: UpdateNoteDto) {
    try {
      return await this.notesRepository.update(
        {
          id: new ObjectID(id),
        },
        updateNoteDto,
      );
    } catch (error) {
      throw error;
    }
  }

  async remove(id: string) {
    try {
      return await this.notesRepository.delete({
        id: new ObjectID(id),
      });
    } catch (error) {
      throw error;
    }
  }
}
