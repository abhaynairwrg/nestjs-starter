import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UnprocessableEntityException,
  NotFoundException,
} from '@nestjs/common';
import { NotesService } from './notes.service';
import { CreateNoteDto } from './dto/create-note.dto';
import { UpdateNoteDto } from './dto/update-note.dto';

@Controller('notes')
export class NotesController {
  constructor(private readonly notesService: NotesService) {}

  @Post()
  async create(@Body() createNoteDto: CreateNoteDto) {
    try {
      await this.notesService.create(createNoteDto);
    } catch (error) {
      throw error;
    }
  }

  @Get()
  async findAll() {
    try {
      return await this.notesService.findAll();
    } catch (error) {
      throw error;
    }
  }

  @Get(':id')
  async findOne(@Param('id') id: string) {
    try {
      const note = await this.notesService.findOne(id);

      if (!note) {
        throw new NotFoundException();
      }

      return note;
    } catch (error) {
      throw error;
    }
  }

  @Patch(':id')
  async update(@Param('id') id: string, @Body() updateNoteDto: UpdateNoteDto) {
    try {
      // If whole request body is empty, throw error
      if (JSON.stringify(updateNoteDto) === '{}') {
        throw new UnprocessableEntityException(
          'Please provide any field values to update',
        );
      }

      const noteExists = await this.notesService.findOne(id);

      if (!noteExists) {
        throw new NotFoundException();
      }

      // update the document
      await this.notesService.update(id, updateNoteDto);
    } catch (error) {
      throw error;
    }
  }

  @Delete(':id')
  async remove(@Param('id') id: string) {
    try {
      const noteExists = await this.notesService.findOne(id);

      if (!noteExists) {
        throw new NotFoundException();
      }

      // delete the document
      await this.notesService.remove(id);
    } catch (error) {
      throw error;
    }
  }
}
